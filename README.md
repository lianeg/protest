# README #

The PROTEST test suite is described in:

*Liane Guillou and Christian Hardmeier. (2016). PROTEST: A Test Suite for Evaluating Pronouns in Machine Translation. In Proceedings of the Tenth International Conference on Language Resources and Evaluation. Portoroz, Slovenia, May 2016.*


For information on how to use PROTEST, please consult the user guide:

**Documentation/PROTEST_user_guide.pdf**