#! /usr/bin/php
<?php

$db_file = "sqlite:protestsuite.db";


# Error handling and reporting
function errh($errno, $errstr, $errfile, $errline) {
	echo "{$errfile} ({$errline}): {$errstr}\n";
	die();
}

function exch($ex) {
	echo "Line " . $ex->getLine() . ": " . $ex->getMessage() . "\n";
	die();
}

set_error_handler('errh');
set_exception_handler('exch');

error_reporting(E_ALL | E_STRICT);


# Check usage of the script (src/tgt)
function usage() {
	global $progname;
	exit("Usage: {$progname} {tgtid refid}\n");
}


# Get the pronoun candidate / antecedent(s)
function get_pro_ant($corpusid, $command, $example_no){
	global $db;
	$get_pro_ants = $db->prepare($command);
	$get_pro_ants->execute(array("tgtcorpus" => $corpusid, "example_no" => $example_no));
	$result = $get_pro_ants->fetchall();
	return $result;
}


# Get the translation(s) of the pronoun/antecedent (as word positions)
function get_translation($corpusid, $command, $example_no, $sentence, $ant_no){
	global $db;
	$string = "";
	$get_trans = $db->prepare($command);
	if(is_null($ant_no))
		$get_trans->execute(array("tgtcorpus" => $corpusid, "example_no" => $example_no));
	else
		$get_trans->execute(array("tgtcorpus" => $corpusid, "example_no" => $example_no, "ant_no" => $ant_no));
	$result = $get_trans->fetchall();
	foreach($result as $r){
		$tok = $sentence[$r["tgtpos"]];
		$string .= $tok." ";
	}
	return trim($string);
}


# Make a searchable array of antecedents
function get_searchable_array($ant_array){
	$result = array();
	foreach($ant_array as $ant){
		$ant_no = $ant["ant_no"];
		$line = $ant["line"];
		$ant_head = $ant["srchead"];
		$result[] = $ant_no;
		$result[$ant_no] = array();
		$result[$ant_no][] = $line;
		$result[$ant_no][] = $ant_head;
	}
	return $result;
}


# Check if reference and SMT translations match (compare lowercased strings)
function check_match($REF_trans, $SMT_trans){
	$result = 0; # Mismatch
	if($REF_trans != "" and $SMT_trans != "" and strpos(strtolower($SMT_trans),strtolower($REF_trans)) !== false) # Check if $REF_trans in $SMT_trans
		$result = 1;
	return $result;
}


# Get nested array of array[line][sentence_tok] for tgt sentence
function get_tgt_sentences($corpusid){
	global $db;
	$sents = array();
	$get_sentences = $db->prepare("select * from sentences where corpus=:tgtcorpus;");
	$get_sentences->execute(array("tgtcorpus" => $corpusid));
	$result = $get_sentences->fetchall();
	foreach($result as $r){
		$lineno = $r["line"];
		$sents[] = $lineno;
		$tgttok = explode(" ", $r["sentence"]);
		$sents[$lineno] = array();
		foreach($tgttok as $t){
			$sents[$lineno][] = $t;
		}
	}
	return $sents;
}


function get_ref_ant($REFcorpus, $command_ants, $example_no){
	global $REF_tgt_sentences;
	$REF_ant_candidates = get_pro_ant($REFcorpus, $command_ants, $example_no);
	$REF_ants = get_searchable_array($REF_ant_candidates); # $REF_ants[ant_no][ant_line][ant_head]
	$ref_ant_no_list = range(1, count($REF_ant_candidates));
	$src_ant = "";
	foreach($ref_ant_no_list as $r_ant){
		$ant_no = $r_ant;
		$ant_head = $REF_ants[$ant_no][1];
		$src_ant .= $ant_head.";";
	}
	$src_ant = rtrim($src_ant, ';');
	return $src_ant;
}


# Check that all of the antecedent head translations match the reference translations
function check_antecedent_matches($REFcorpus, $SMTcorpus, $command_ants, $command_ant_trans, $example_no){
	global $REF_tgt_sentences;
	global $SMT_tgt_sentences;
	# Get the SMT and REF pro_antecedent translation
	$REF_ant_candidates = get_pro_ant($REFcorpus, $command_ants, $example_no);
	$SMT_ant_candidates = get_pro_ant($SMTcorpus, $command_ants, $example_no);
	# Make searchable
	$REF_ants = get_searchable_array($REF_ant_candidates); # $REF_ants[ant_no][ant_line][ant_head]
	$SMT_ants = get_searchable_array($SMT_ant_candidates);
	# Check that each antecedent matches - if one mismatch (non-translation) found then pro-ant pair is a mismatch
	$ref_ant_no_list = range(1, count($REF_ant_candidates));
	$ant_match = 1;
	$result_string = "match";
	$trans_ant = "";
	$ref_ant = "";
	$src_ant = "";
	foreach($ref_ant_no_list as $r_ant){
		$ant_no = $r_ant;
		$ant_line = $REF_ants[$ant_no][0];
		$ant_head = $REF_ants[$ant_no][1];
		$src_ant .= $ant_head.";";
		# Get translation of REF ant head
		$REF_ant_trans = get_translation($REFcorpus, $command_ant_trans, $example_no, $REF_tgt_sentences[$ant_line], $ant_no);
		$ref_ant .= $REF_ant_trans.";";
		# Check if pro_antecedent exists in $SMT_ants
		if(array_key_exists($ant_no, $SMT_ants)){
			# Get translation of SMT ant head
			$SMT_ant_trans = get_translation($SMTcorpus, $command_ant_trans, $example_no, $SMT_tgt_sentences[$ant_line], $ant_no);
			$trans_ant .= $SMT_ant_trans.";";
			# Check if antecedent head translations match
			$match = check_match($REF_ant_trans, $SMT_ant_trans);
			if($match==0){
				$ant_match = 0;
				if($SMT_ant_trans=="")
					$result_string = "antecedent not translated in SMT";
				else
					$result_string = "mismatch";
			}   
		}
		else{
			$ant_match = 0;
			$result_string = "antecedent not translated in SMT";
			$trans_ant .= ";";
		}
	}
	return array($result_string,$src_ant,$ref_ant,$trans_ant,$ant_match);
}


# Get SMT and reference corpus IDs from the command line input
$progname = array_shift($argv);
$SMTcorpus = $argv[0];
$REFcorpus = $argv[1];
$outfile = $argv[2];
if(!is_numeric($SMTcorpus) || !is_numeric($REFcorpus))
	usage();


# Query the DB

$db = new PDO($db_file);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$db->beginTransaction();


# SET-UP ARRAYS AND COMMANDS USED IN THE COMPARISON

# Get pronoun categories
$categories = array(); # $categories[id] = [description,antagreement]
$get_cats = $db->prepare("select id, description, antagreement from categories");
$get_cats->execute();
$cats = $get_cats->fetchall();
foreach($cats as $c){
	$categories[$c["id"]] = array();
	$categories[$c["id"]][] = $c["description"];
	$categories[$c["id"]][] = $c["antagreement"];
}

# Get list of all pronoun candidates
$get_pro_ant_nos = $db->prepare("select example_no from pro_candidates where tgtcorpus=:tgtcorpus;");
$get_pro_ant_nos->execute(array("tgtcorpus" => $REFcorpus));
$example_nos = $get_pro_ant_nos->fetchall();

# Command for extracting pro_candidates and pro_antecedents
$command_pros = "select srcpro, srcpos, category_no, line from pro_candidates where tgtcorpus=:tgtcorpus and example_no=:example_no;";
$command_ants = "select srchead, ant_no, line from pro_antecedents where tgtcorpus=:tgtcorpus and example_no=:example_no;";

# Commands for extracting translations of pro_candidate and pro_antecedent (as word positions)
$command_pro_trans = "select ant_no, line, tgtpos from translations where tgtcorpus=:tgtcorpus and example_no=:example_no and ant_no IS NULL order by ant_no, tgtpos;";
$command_ant_trans = "select ant_no, line, tgtpos from translations where tgtcorpus=:tgtcorpus and example_no=:example_no and ant_no=:ant_no order by tgtpos";

# Get array of SMT target sentences of the format array[line][tok1,...,tokN]
$SMT_tgt_sentences = get_tgt_sentences($SMTcorpus);

# Get array of REF target sentences of the format array[line][tok1,...,tokN]
$REF_tgt_sentences = get_tgt_sentences($REFcorpus);



# PERFORM COMPARISON

# Store results
$eval_results = array(); # $eval_results[category_no][example_no] = [srcpos,line,srcpro,ref_pro,trans pro,src ant,ref_ant,trans ant,result]
$cat_no_list = (array_keys($categories));
foreach($cat_no_list as $c){
	$eval_results[] = $c;
	$eval_results[$c] = array();
}

# Loop through all example numbers
foreach($example_nos as $ex_no){
	$example_no = $ex_no["example_no"];
	# Get the SMT and REF pro_candidate
	$REF_pro_candidate = get_pro_ant($REFcorpus, $command_pros, $example_no);
	$SMT_pro_candidate = get_pro_ant($SMTcorpus, $command_pros, $example_no);
	$category_no = $REF_pro_candidate[0]["category_no"];
	$pro_line = $REF_pro_candidate[0]["line"];
	# Get reference translation of pronoun
	$REF_pro_trans = get_translation($REFcorpus, $command_pro_trans, $example_no, $REF_tgt_sentences[$pro_line], NULL);
	# Fill the results with standard info from the $REF_pro_candidate
	$eval_results[$category_no][] = $example_no;
	$eval_results[$category_no][$example_no] = array();
	$eval_results[$category_no][$example_no][] = $REF_pro_candidate[0]["srcpos"]; # 0 = srcpos
	$eval_results[$category_no][$example_no][] = $REF_pro_candidate[0]["line"]; # 1 = line
	$eval_results[$category_no][$example_no][] = $REF_pro_candidate[0]["srcpro"]; # 2 = srcpro
	$eval_results[$category_no][$example_no][] = rtrim($REF_pro_trans, ';'); # 3 = ref_pro
	$antagreement = $categories[$category_no][1];
	if(!empty($SMT_pro_candidate)){ # Pronoun candidate found in SMT
		# Get SMT translation of pronoun
		$SMT_pro_trans = get_translation($SMTcorpus, $command_pro_trans, $example_no, $SMT_tgt_sentences[$pro_line], NULL);
		# Fill "trans pro" in results
		$eval_results[$category_no][$example_no][] = $SMT_pro_trans; # 4 = trans pro
		if($antagreement==1){ # Antecedent agreement required for this pronoun
			# Check if the pronoun translations match
			$pro_match = check_match($REF_pro_trans, $SMT_pro_trans);
			# Check if all pronoun antecedent translations match
			$checks = check_antecedent_matches($REFcorpus, $SMTcorpus, $command_ants, $command_ant_trans, $example_no);
			$result_string = $checks[0];
			$src_ant = rtrim($checks[1], ';'); 
			$ref_ant = rtrim($checks[2], ';'); 
			$trans_ant = rtrim($checks[3], ';'); 
			$ant_match = $checks[4];
			# Check if pronoun and antecedent(s) are all matches
			if($SMT_pro_trans=="" and $result_string=="antecedent not translated in SMT")
				$result_string = "pronoun and antecedent not translated by SMT";
			elseif($SMT_pro_trans=="")
				$result_string = "pronoun not translated by SMT";
			elseif($result_string=="match" and ($ant_match==0 || $pro_match==0))
				$result_string = "mismatch";
			$eval_results[$category_no][$example_no][] = $src_ant; # 5 = src ant
			$eval_results[$category_no][$example_no][] = $ref_ant; # 6 = ref ant
			$eval_results[$category_no][$example_no][] = $trans_ant; # 7 = trans ant
			$eval_results[$category_no][$example_no][] = $result_string; # 8 = result
		}
		else{ # Antecedent agreement NOT required for this pronoun
			# Fill "trans ant" in results
			$eval_results[$category_no][$example_no][] = ""; # 5 = src ant (no antecedent)
			$eval_results[$category_no][$example_no][] = ""; # 6 = ref ant (no antecedent)
			$eval_results[$category_no][$example_no][] = ""; # 7 = trans ant (no antecedent)
			# Check if the pronoun translations match
			$pro_match = check_match($REF_pro_trans, $SMT_pro_trans);
			if($pro_match==1)
				$eval_results[$category_no][$example_no][] = "match"; # 8 = result
			else{
				if($SMT_pro_trans=="")
					$eval_results[$category_no][$example_no][] = "pronoun not translated by SMT"; # 7 = result
				else 
					$eval_results[$category_no][$example_no][] = "mismatch"; # 8 = result
				}
		} 
	}
	else{ # Pronoun candidate NOT FOUND in SMT (mark as not found)
		# Fill remainer of results
		$eval_results[$category_no][$example_no][] = ""; # 4 = trans pro
		if($antagreement==1){
			$src_ant = get_ref_ant($REFcorpus, $command_ants, $example_no);
			$eval_results[$category_no][$example_no][] = $src_ant; # 5 = src ant
		}
		else
			$eval_results[$category_no][$example_no][] = ""; # 5 = src ant
		$eval_results[$category_no][$example_no][] = ""; # 6 = ref ant
		$eval_results[$category_no][$example_no][] = ""; # 7 = trans ant
		$eval_results[$category_no][$example_no][] = "pronoun not translated by SMT"; # 8 = result
	}
}



# OUTPUT STATS

# Open file for output
$output = fopen($outfile,"w");
fwrite($output,"category\texample_no\tsrc_word\tsrc_sentence\tsrc_pronoun\tref_pronoun\tpro_translation\tsrc_antecedent\tref_antecedent\tant_translation\tresult\n");

# Loop through the categories
$total_match = 0;
$total_mismatch = 0;
$grand_total = 0;
foreach($eval_results as $category_no => $cat_no){
	if(is_array($cat_no)){
		echo $categories[$category_no][0]."\n";
		# Loop through the pronoun candidates belonging to each category
		$count_match = 0;
		$count_mismatch = 0;
		foreach ($cat_no as $example_no => $ex_no){
			if(is_array($ex_no)){
				fwrite($output,$categories[$category_no][0]."\t".$example_no."\t".$ex_no[0]."\t".$ex_no[1]."\t".$ex_no[2]."\t".$ex_no[3]."\t".$ex_no[4]."\t".$ex_no[5]."\t".$ex_no[6]."\t".$ex_no[7]."\t".$ex_no[8]."\n");
				if($ex_no[8]=="match")
					$count_match++;
				else
					$count_mismatch++;
			}
		}
		$sub_total = $count_match + $count_mismatch;
		$grand_total += $sub_total;
		$total_match += $count_match;
		$total_mismatch += $count_mismatch;
		print "  Matches: ".$count_match."\n";
		print "  Mismatches: ".$count_mismatch."\n";
		print "  TOTAL: ".$sub_total."\n";
	}
}
print "---------------\n";
print "  TOTAL Matches: ".$total_match."\n";
print "  TOTAL Mismatches: ".$total_mismatch."\n";
print "  GRAND TOTAL: ".$grand_total."\n";

# Close output file
fclose($output);

?>
