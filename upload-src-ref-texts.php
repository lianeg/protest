#! /usr/bin/php

<?php
# Script to upload the source and reference texts

$db_file = "sqlite:protestsuite.db";


# Error handling and reporting
function errh($errno, $errstr, $errfile, $errline) {
	echo "{$errfile} ({$errline}): {$errstr}\n";
	die();
}

function exch($ex) {
	echo "Line " . $ex->getLine() . ": " . $ex->getMessage() . "\n";
	die();
}

set_error_handler('errh');
set_exception_handler('exch');

error_reporting(E_ALL | E_STRICT);


# Check usage of the script (src/ref)
function usage() {
	global $progname;
	exit("Usage: {$progname} {-s|-t srcid} file.xml\n");
}

# Original source texts are given the value "0"; reference texts are given the value "1"
# Corresponds to "usage" of the script
$progname = array_shift($argv);
switch(array_shift($argv)) {
	case "-s":
		$srctgt = 0;
		break;
	case "-t":
		$srctgt = 1;
		$srccorpus = array_shift($argv);
		if(!is_numeric($srccorpus))
			usage();
		break;
	default:
		usage();
}

# Check that the script is being used correctly, with the correct number of arguments
if($srctgt == 1 && count($argv) != 1 || $srctgt == 0 && count($argv) != 1)
	usage();


# Input file specified in argument variable 0
$infile = $argv[0];


# Insert document into DB

$db = new PDO($db_file);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$find_corpus = $db->prepare("select id from corpora where name=:name");
$find_corpus->execute(array("name" => $infile));

$db->beginTransaction();

# Load the XML file
$doc = new DOMDocument();

if(!($doc->load($infile))) {
	echo "Error loading " . $infile;
	$db->rollBack();
	exit(1);
}

# Prepare to insert corpus entry to table: corpora
$insert_corpus = $db->prepare("insert into corpora(name, srctgt) values (:name, :srctgt)");

# Prepare to insert sentence entries to table: sentences
$insert_sentence = $db->prepare("insert into sentences(corpus, line, sentence) values (:corpus, :line, :sentence)");

# Make the corpus entry and insert
$insert_corpus->execute(array("name" => $infile, "srctgt" => $srctgt));
$corpusid = $db->lastInsertId();
echo "Corpus id: {$corpusid}\n";

# Read the XML file and find the segments (i.e. sentences)
$xpath = new DOMXPath($doc);
$segs = $xpath->query("//seg");

# Make the sentence entries and insert
if($srctgt == 1) {
	$get_srclines = $db->prepare("select sentence from sentences where corpus=:srccorpus order by line");
	$get_srclines->execute(array("srccorpus" => $srccorpus));
}

$record = array("corpus" => $corpusid);
$lineno = 0;
foreach($segs as $segnode) {
    $lineno++;
	$line = $segnode->nodeValue;
	$record["line"] = $lineno;
	$record["sentence"] = trim($line);
	$insert_sentence->execute($record);

	if($srctgt == 1) {
		$srcline = $get_srclines->fetch();
		$srctok = explode(" ", $srcline["sentence"]);
	}
}

# Prepare and insert document start/end entries to table: documents
$docs = $xpath->query("//doc");
$docstart = 0;
$insert_doc = $db->prepare("insert into documents(corpus, start, end) values (:corpus, :start, :end)");
foreach($docs as $docnode) {
	$cnt = $xpath->evaluate("count(.//seg)", $docnode);
	$insert_doc->execute(array("corpus" => $corpusid, "start" => $docstart, "end" => $docstart + $cnt - 1));
	$docstart += $cnt;
}

# Commit changes to the database
$db->commit();
#$db->rollBack();

?>
