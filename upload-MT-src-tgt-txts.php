#! /usr/bin/php

<?php
# Script to upload src and SMT output texts (and find their translations of each pronoun)

$db_file = "sqlite:protestsuite.db";


# Error handling and reporting
function errh($errno, $errstr, $errfile, $errline) {
	echo "{$errfile} ({$errline}): {$errstr}\n";
	die();
}

function exch($ex) {
	echo "Line " . $ex->getLine() . ": " . $ex->getMessage() . "\n";
	die();
}

set_error_handler('errh');
set_exception_handler('exch');

error_reporting(E_ALL | E_STRICT);


# Get tgt translation of src word
function get_alignment($srcnode) {
	global $alighandle;
	$aligline = trim(fgets($alighandle));

	if(empty($aligline))
		return array();

	$aa = explode(" ", trim($aligline));
	$alig = array();
	foreach($aa as $a) {
		$l = explode("-", $a);
		$alig[$l[0]][] = $l[1];
	}
	return $alig;
}


# Insert pronoun (table: pro_candidates)
function insert_pro_candidate($example_no, $ant_no, $category_no, $srcindex, $line, $srcpos, $srcpro, $alig, $tgtline, $sequence_no) {
	global $insert_pro_candidate;
	global $srccorpus;
	global $corpusid;
	# Insert src pronoun into table: pro_candidates
	$insert_pro_candidate->execute(array("srccorpus" => $srccorpus, "tgtcorpus" => $corpusid, "example_no" => $example_no,
		"line" => $line, "srcpro" => $srcpro, "srcpos" => $srcpos, "srcproindex" => $srcindex, "category_no" => $category_no, "sequence_no" => $sequence_no));
    # Insert translations
	insert_translation($example_no, $ant_no, $line, $srcpos, $alig, $tgtline);
}


# Insert antecedent head (table: pro_antecedent)
function insert_ant_candidate($example_no, $ant_no, $srcindex, $line, $srcstartpos, $srcendpos,$srcheadpos, $srchead, $alig, $tgtline) {
	global $insert_ant_candidate;
	global $srccorpus;
	global $corpusid;
	# Insert src pronoun into table: pro_antecedents
	$insert_ant_candidate->execute(array("srccorpus" => $srccorpus, "tgtcorpus" => $corpusid, "example_no" => $example_no, "ant_no" => $ant_no, "line" => $line, "srcstartpos" => $srcstartpos, "srcendpos" => $srcendpos, "srchead" => $srchead, "srcheadpos" => $srcheadpos, "srcantheadindex" => $srcindex));
	# Insert translations
	insert_translation($example_no, $ant_no, $line, $srcheadpos, $alig, $tgtline);
}


# Insert translation (table: translations)
function insert_translation($example_no, $ant_no, $line, $srcpos, $alig, $tgtline){
    global $corpusid;
    global $insert_translation;
	$tgt = explode(" ", trim($tgtline));
	if(array_key_exists($srcpos, $alig)) {
		foreach($alig[$srcpos] as $a)
		    # Insert translation(s) into table: translations
		    $insert_translation->execute(array("tgtcorpus" => $corpusid, "example_no" => $example_no, "ant_no" => $ant_no, "line" => $line, "tgtpos" => $a));
	}    
}


# Check if the token is a pronoun / antecedent head that matches one of the examples (i.e. a "candidate")
# If so, insert it and its translation
function check_if_candidate_and_insert($srctok, $i, $line, $lineno, $listindex, $list_tokens, $list_token_indices, $alig, $command, $entry_type){
    global $origsrccorpus;
    global $db;
    global $candidate_pro_counter;
    global $candidate_ant_head_counter;
	global $seq_nos;
    $srctoken = strtolower($srctok[$i]);
    $listindex = array_search($srctoken, $list_tokens);
    $srctokenindex = $list_token_indices[$listindex];
    # Find the corresponding antecedent head from the original src text:
    $get_gold_example = $db->prepare($command);
    if($entry_type == "antecedent"){
        $get_gold_example->execute(array("origsrccorpus" => $origsrccorpus, "line" => $lineno, "srchead" => $srctoken, "srcantheadindex" => $srctokenindex));
    }
    else{# Pronoun
        $get_gold_example->execute(array("origsrccorpus" => $origsrccorpus, "line" => $lineno, "srcpro" => $srctoken, "srcproindex" => $srctokenindex));
    }
	$gold_example_record = $get_gold_example->fetchall();
	# There may be multiple pro-antecedent pairs sharing the same antecedent head
	foreach ($gold_example_record as $g) {
        # If the antecedent head matches one of the heads in pro_antecedents for the original source corpus
		$example_no = $g["example_no"];
		if($entry_type == "antecedent"){
		    $srcstartpos = $g["srcstartpos"];
		    $srcendpos = $g["srcendpos"];
		    $ant_no = $g["ant_no"];
		    # Create the pro_ant_head_example
            insert_ant_candidate($example_no, $ant_no, $srctokenindex, $lineno, $srcstartpos, $srcendpos, $i, $srctoken, $alig, trim($line));
            $candidate_ant_head_counter++;
        }
        else{# Pronoun
		    $category_no = $g["category_no"];
		    # Create the pro_candidate
			$sequence_no = array_shift($seq_nos);
            insert_pro_candidate($example_no, NULL, $category_no, $srctokenindex, $lineno, $i, $srctoken, $alig, trim($line), $sequence_no);
            $candidate_pro_counter++;
        }
	}
}
   

# Get a list of words used in the examples (these may be pronouns or antecedent heads)
function get_list_words($command) {
    global $origsrccorpus;
    global $db;
	$list_words = array();
    $get_words_list = $db->prepare($command);
    $get_words_list->execute(array("origsrccorpus" => $origsrccorpus));
    $gold_words_list = $get_words_list->fetchall();
    foreach ($gold_words_list as $g) {
        $list_words[] = strtolower($g[0]);
    }
    return $list_words;
}


# Check usage of the script (src/tgt)
function usage() {
	global $progname;
	exit("Usage: {$progname} {-s|-t srcid origsrcid} file.xml file.alig\n");
}

# Original source texts are given the value "0"; reference texts are given the value "1"
# Corresponds to "usage" of the script
$progname = array_shift($argv);
switch(array_shift($argv)) {
	case "-s":
		$srctgt = 0;
		break;
	case "-t":
		$srctgt = 1;
		$srccorpus = array_shift($argv);
		$origsrccorpus = array_shift($argv);
		if(!is_numeric($srccorpus) || !is_numeric($origsrccorpus))
			usage();
		break;
	default:
		usage();
}

# Check that the script is being used correctly, with the correct number of arguments
if($srctgt == 1 && count($argv) != 2 || $srctgt == 0 && count($argv) != 1)
	usage();

# Input file specified in argument variable 0
$infile = $argv[0];

if($srctgt == 1) {
	$aligfile = $argv[1];
	$alighandle = fopen($aligfile, "r");
}

# Insert document into DB

$db = new PDO($db_file);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$find_corpus = $db->prepare("select id from corpora where name=:name");
$find_corpus->execute(array("name" => $infile));

$db->beginTransaction();

# Load the XML file
$doc = new DOMDocument();

if(!($doc->load($infile))) {
	echo "Error loading " . $infile;
	$db->rollBack();
	exit(1);
}

# Prepare to insert corpus entry to table: corpora
$insert_corpus = $db->prepare("insert into corpora(name, srctgt) values (:name, :srctgt)");

# Prepare to insert sentence entries to table: sentences
$insert_sentence = $db->prepare("insert into sentences(corpus, line, sentence) values (:corpus, :line, :sentence)");

# Prepare to insert pronoun candidate entries to table: pro_candidates
$insert_pro_candidate = $db->prepare("insert into pro_candidates(srccorpus, tgtcorpus, example_no, line, srcpro, srcpos, srcproindex, category_no, sequence_no) values (:srccorpus, :tgtcorpus, :example_no, :line, :srcpro, :srcpos, :srcproindex, :category_no, :sequence_no)");

# Prepare to insert antecedent entries to table: pro_antecedents
$insert_ant_candidate = $db->prepare("insert into pro_antecedents(srccorpus, tgtcorpus, example_no, ant_no, line, srcstartpos, srcendpos, srchead, srcheadpos, srcantheadindex) values (:srccorpus, :tgtcorpus, :example_no, :ant_no, :line, :srcstartpos, :srcendpos, :srchead, :srcheadpos, :srcantheadindex)");

# Prepare to insert translation entries to table: translation
$insert_translation = $db->prepare("insert into translations(tgtcorpus, example_no, ant_no, line, tgtpos) values (:tgtcorpus, :example_no, :ant_no, :line, :tgtpos)");

# Make the corpus entry and insert
$insert_corpus->execute(array("name" => $infile, "srctgt" => $srctgt));
$corpusid = $db->lastInsertId();
echo "Corpus id: {$corpusid}\n";

# Read the XML file and find the segments (i.e. sentences)
$xpath = new DOMXPath($doc);
$segs = $xpath->query("//seg");

# Make the sentence entries and insert
if($srctgt == 1) {
	$get_srclines = $db->prepare("select sentence from sentences where corpus=:srccorpus order by line");
	$get_srclines->execute(array("srccorpus" => $srccorpus));
}

$record = array("corpus" => $corpusid);
$lineno = 0;
# Dynamically create lists of pronouns and antecedent head words from pro_candidates
if($srctgt == 1) {
    # Pronouns
    $command = "select distinct(srcpro) from pro_candidates where srccorpus=:origsrccorpus";
    $list_pros = get_list_words($command);
    #$list_pro_indices = array_fill(0, count($list_pros), 0);
    # Antecedent heads
    $command = "select distinct(srchead) from pro_antecedents where srccorpus=:origsrccorpus";
    $list_ant_heads = get_list_words($command);
    #$list_ant_head_indices = array_fill(0, count($list_ant_heads), 0);
}
# Go through each sentence and find occurences of the pronoun and antecedent head words
$candidate_pro_counter = 0;
$candidate_ant_head_counter = 0;
# Get example_no count for randomisation
$get_count_example_nos = $db->prepare("select count(distinct(example_no)), max(sequence_no) from pro_candidates");
$get_count_example_nos->execute(array());
$example_no_info = $get_count_example_nos->fetchall();
$count_example_no = $example_no_info[0][0];
$max_seq_no = $example_no_info[0][1];
# Produce list of numbers 1...X and randomise (for sequence_no field)
$seq_nos = range($max_seq_no+1, $max_seq_no+$count_example_no);
shuffle($seq_nos);
foreach($segs as $segnode) {
    $lineno++;
	$line = $segnode->nodeValue;
	$record["line"] = $lineno;
	$record["sentence"] = trim($line);
	$insert_sentence->execute($record);
	if($srctgt == 1) {
	    $list_pro_indices = array_fill(0, count($list_pros), 0);
	    $list_ant_head_indices = array_fill(0, count($list_ant_heads), 0);
		$alig = get_alignment($segnode);
		$srcline = $get_srclines->fetch();
		$srctok = explode(" ", $srcline["sentence"]);
	    # Loop over each src token
		for($i = 0; $i < count($srctok); $i++)
		    # Look for instances of the pronouns / antecedent head words that we are interested in
            if(in_array(strtolower($srctok[$i]), $list_pros)){
                $command = "select * from pro_candidates as e where e.srccorpus=:origsrccorpus and e.line=:line and e.srcpro=:srcpro and e.srcproindex=:srcproindex";
                $srcpro = strtolower($srctok[$i]);
                $prolistindex = array_search($srcpro, $list_pros);
                # Insert pronoun candidate
                check_if_candidate_and_insert($srctok, $i, $line, $lineno, $prolistindex, $list_pros, $list_pro_indices, $alig, $command, "pronoun");
                # Update the index to reflect that a token matching the one that we're interested in, was found
                $list_pro_indices[$prolistindex]++;
            }
            elseif(in_array(strtolower($srctok[$i]), $list_ant_heads)){
                $command = "select * from pro_antecedents as a where a.srccorpus=:origsrccorpus and a.line=:line and a.srchead=:srchead and a.srcantheadindex=:srcantheadindex";
                $srchead = strtolower($srctok[$i]);
                $antlistindex = array_search($srchead, $list_ant_heads);
                # Insert antecedent for pronoun candidate
                check_if_candidate_and_insert($srctok, $i, $line, $lineno, $antlistindex, $list_ant_heads, $list_ant_head_indices, $alig, $command, "antecedent");
                # Update the index to reflect that a token matching the one that we're interested in, was found
                $list_ant_head_indices[$antlistindex]++;
            }
	}
}

# Prepare and insert document start/end entries to table: documents
$docs = $xpath->query("//doc");
$docstart = 0;
$insert_doc = $db->prepare("insert into documents(corpus, start, end) values (:corpus, :start, :end)");
foreach($docs as $docnode) {
	$cnt = $xpath->evaluate("count(.//seg)", $docnode);
	$insert_doc->execute(array("corpus" => $corpusid, "start" => $docstart, "end" => $docstart + $cnt - 1));
	$docstart += $cnt;
}

# Commit changes to the database
$db->commit();
#$db->rollBack();

if($srctgt == 1){
	fclose($alighandle);
    global $origsrccorpus;
    # Get total number of pronoun examples in test suite:
    $get_pro_count = $db->prepare("select count(*) from pro_candidates where srccorpus=:origsrccorpus");
    $get_pro_count->execute(array("origsrccorpus" => $origsrccorpus));
    $pro_test_suite_count = $get_pro_count->fetch();
    # Get total number of antecedent heads for the pronoun examples in test suite:
    $get_ant_head_count = $db->prepare("select count(*) from pro_antecedents where srccorpus=:origsrccorpus");
    $get_ant_head_count->execute(array("origsrccorpus" => $origsrccorpus));
    $ant_test_suite_count = $get_ant_head_count->fetch();
    # Print results:
    echo "Found: ", $candidate_pro_counter, "/", $pro_test_suite_count[0], " pronouns\n";
    echo "Found: ", $candidate_ant_head_counter, "/", $ant_test_suite_count[0], " antecedent heads\n";
}

?>
